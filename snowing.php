<?php
/**
 * Plugin Name: VCS Snowing
 * Plugin URI:
 * Description: Create a simple snow overlay canvas for your WordPress website.
 * Version: 2.1
 * Author: Nicholas McDonald (version 2.1: Henry Wetton)
 * Author URI: https://weigert.vsos.ethz.ch/
**/

// Add the Tools Page for Toggling Activity
add_action('admin_menu', 'snowing_add_toolpage');

function snowing_add_toolpage() {
    // Add the Management Page Callback
    add_management_page('Snowing', 'Snowing', 'manage_options', __FILE__, 'snowing_tool_page');

    // Add the Plugin Settings
    add_action('admin_init', 'snowing_register_settings');
}

function snowing_register_settings() {
    // Register the settings
    register_setting('snowing-settings-group', 'snowing_active', ['sanitize_callback' => 'absint']);
    register_setting('snowing-settings-group', 'snowing_frontpage', ['sanitize_callback' => 'absint']);
}

function snowing_tool_page() { ?>
    <div class="snowing_tool_container">
        <h1>Snowing Overlay</h1>
        <form method="post" action="options.php">
            <?php settings_fields('snowing-settings-group'); ?>
            <?php do_settings_sections('snowing-settings-group'); ?>
            <div class="snowing_tool_option">
                <h2 class="snowing">Activate the snowing overlay?</h2>
                <input class="snowing" type="checkbox" name="snowing_active" value="1" <?php checked(get_option('snowing_active'), 1); ?>/>
            </div>
            <div class="snowing_tool_option">
                <h2 class="snowing">Only show on front page?</h2>
                <input class="snowing" type="checkbox" name="snowing_frontpage" value="1" <?php checked(get_option('snowing_frontpage'), 1); ?>/>
            </div>
            <?php submit_button(); ?>
        </form>
    </div>
<?php
}

function add_snowing_style() {
    $plugin_url = plugin_dir_url(__FILE__);
    wp_enqueue_style('snowing_style', $plugin_url . 'style.css', array(), '1.0', 'all');
}

function add_snowing_script() {
    $plugin_url = plugin_dir_url(__FILE__);
    wp_register_script('snowing_script', $plugin_url . 'snowing.js', array('jquery'), NULL, true);

    if (get_option('snowing_active') == "1") {
        if (get_option('snowing_frontpage') == "1" && is_front_page()) {
            wp_enqueue_script('snowing_script');
        } else if (get_option('snowing_frontpage') != "1") {
            wp_enqueue_script('snowing_script');
        }
    }
}

// Check whether the date is appropriate (added in version 2.1)
$year_current = getdate()['year'];
$date_current = time();
$date_begin = mktime(0, 0, 0, 12, 1, $year_current);
$date_end = mktime(23, 59, 59, 1, 6, $year_current + 1);

if ($date_current >= $date_begin || $date_current <= $date_end) {
    add_action('wp_enqueue_scripts', 'add_snowing_script');
    add_action('wp_enqueue_scripts', 'add_snowing_style');
    add_action('admin_enqueue_scripts', 'add_snowing_style');
}
